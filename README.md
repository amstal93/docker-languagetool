![](https://images.microbadger.com/badges/version/jarylc/languagetool.svg) ![](https://images.microbadger.com/badges/image/jarylc/languagetool.svg) ![](https://img.shields.io/docker/stars/jarylc/languagetool.svg) ![](https://img.shields.io/docker/pulls/jarylc/languagetool.svg)

This is a fork of [Erikvl87/docker-languagetool](https://github.com/Erikvl87/docker-languagetool)

# Environment variables:
| Environment | Default value |
|-------------|---------------|
| PORT        | 8081          |
| Java_Xms    | 256m          |
| Java_Xmx    | 512m          |

For `langtool_*` environment variables and ngrams setup documentation, visit: [Erikvl87/docker-languagetool/master/README.md](https://github.com/Erikvl87/docker-languagetool/blob/master/README.md)

# Deploying
## Terminal
```bash
docker run -d \
    --name languagetool \
    -e PORT=8010 \
    -e Java_Xms=256m `# OPTIONAL: Setting a minimal Java heap size of 256 mb` \
    -e Java_Xmx=512m `# OPTIONAL: Setting a maximum Java heap size of 512 mb` \
    -e langtool_languageModel=/ngrams `# OPTIONAL: Using ngrams data` \
    -v /path/to/ngrams:/ngrams `# OPTIONAL: Using ngrams data` \
    -p 8010:8010 \
    --restart unless-stopped \
    jarylc/languagetool
```
## Docker-compose
```yml
languagetool:
    image: jarylc/languagetool
    ports:
        - "8010:8010"
    volumes:
        - /path/to/ngrams:/ngrams # OPTIONAL: Using ngrams data
    environment:
        - PORT=8010
        - Java_Xms=256m # OPTIONAL: Setting a minimal Java heap size of 256 mb
        - Java_Xmx=512m # OPTIONAL: Setting a maximum Java heap size of 512 mb
        - langtool_languageModel=/ngrams # OPTIONAL: Using ngrams data
    restart: unless-stopped
```
