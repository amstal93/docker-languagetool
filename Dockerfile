FROM debian:buster as build
ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update -y && \
    apt-get install -y \
            locales \
            libgomp1 \
            openjdk-11-jdk-headless \
            git \
            maven \
            unzip \
            xmlstarlet && \
    apt-get clean

ARG LGT_VER
RUN sed -i -e 's/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen && \
    dpkg-reconfigure --frontend=noninteractive locales && \
    update-locale LANG=en_US.UTF-8 && \
    cd /tmp && git clone https://github.com/languagetool-org/languagetool.git --depth 1 -b ${LGT_VER}

ENV LANG en_US.UTF-8
WORKDIR /tmp/languagetool
RUN ["mvn", "--projects", "languagetool-standalone", "--also-make", "package", "-DskipTests", "--quiet"]
RUN LGT_DIST_VER=$(xmlstarlet sel -N "x=http://maven.apache.org/POM/4.0.0" -t -v "//x:project/x:properties/x:revision" pom.xml) && unzip /tmp/languagetool/languagetool-standalone/target/LanguageTool-${LGT_DIST_VER}.zip -d /dist && \
    LGT_DIST_DIR=$(find /dist/ -name 'LanguageTool-*') && mv $LGT_DIST_DIR/* /dist/

FROM alpine

ENV PORT=8010 \
    Java_Xms=256m \
    Java_Xmx=512m

RUN apk add bash openjdk11-jre-headless && \
    addgroup -S lgt && adduser -S lgt -G lgt
COPY --chown=lgt --from=build /dist /app
COPY --chown=lgt entrypoint.sh config.properties /app/

USER lgt
WORKDIR /app
ENTRYPOINT ["/app/entrypoint.sh"]
