#!/bin/bash

apk add curl jq

[[ ! -f EXISTING_LGT ]] || touch EXISTING_LGT
EXISTING_LGT=$(cat EXISTING_LGT)
echo "Existing LanguageTool: ${EXISTING_LGT}"

if [[ -n $OVERWRITE_LGT ]]; then
  echo "Overwriting QBT: $OVERWRITE_LGT"
  LATEST_LGT=$OVERWRITE_LGT
else
  LATEST_LGT=$(curl -ks https://api.github.com/repos/languagetool-org/languagetool/git/refs/tags | jq -r '.[-1].ref' | cut -d'/' -f3)
  echo "Latest LanguageTool: ${LATEST_LGT}"
fi

if [[ (-n "${LATEST_LGT}" && "${LATEST_LGT}" != "${EXISTING_LGT}") ]]; then
  echo "${LATEST_LGT}" > LATEST_LGT
  echo "Building..."
fi
